## Conversion and Evaluation of different kinds of expressions, implementation using stack
A java project to implement the data structures mainly the use of Stacks without using library
(Only generic java programming)

## User Manual
#### Starting the application
* The user must go to testerClass where the runnable class are located
* For more information about the project, you can go to the Documentation package and click README.md

#### A brief overview:
* Acknoledgement: The members in the picture all contributed to create this succesful program
* (![](https://gitlab.com/2227046/cs211_9328_group5_midterm/-/blob/main/src/Documentation/Stack_documentation/Intro_members.png))
* Menu of the program
* ![](Stack_documentation/Menu.png)
* When user enter 1
* ![](Stack_documentation/input1.1.png)
* ![](Stack_documentation/input1.2.png)
* When user input 2
* ![](Stack_documentation/input2.0.png)
* When user input 3
* ![](Stack_documentation/input3.0.png)
* When user input 4
* ![](Stack_documentation/input4.0.png)
